package com.niit.auth.controller;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.niit.auth.model.User;
import com.niit.auth.service.UserDAO;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("/api/authen")
public class UserController {

	@Autowired
	UserDAO userservice;
	@PostMapping("/adduser")
	public ResponseEntity add(@RequestBody User usr)
	{
		User user=userservice.addUser(usr);
		return new ResponseEntity<User>(user,HttpStatus.OK);
	}

	@GetMapping("/login")
	public ResponseEntity login(@RequestBody User usr)
	{
		User user=userservice.validate(usr.getUname(), usr.getPassword());
		if(user==null)
		{
			return new ResponseEntity<String>("Invalid credentials",HttpStatus.UNAUTHORIZED);
		}
		
		String tok=generateToken(usr.getUname());
		HashMap<String,String> mymap=new HashMap<String,String>();
	   mymap.put("token", tok);
	   return new ResponseEntity<HashMap>(mymap,HttpStatus.OK);
	}
	
	public String generateToken(String uname)
	
	{
		long expirytime=10_00_0000;
		
		return Jwts.builder().setSubject(uname)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis()+expirytime))
				.signWith(SignatureAlgorithm.HS256,"secretKey")
				.compact();
				
				
    }
	
}