package com.niit.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserAuthorizeApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserAuthorizeApplication.class, args);
	}

}
