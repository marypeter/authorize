package com.niit.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.niit.auth.model.User;
import com.niit.auth.repo.Userrepo;

@Service
public class UserDAOImpl implements UserDAO{

	
	@Autowired
	Userrepo urepo;
	
	@Override
	public User addUser(User obj) {
		 
		User user=urepo.save(obj);
		return user;
	}

	@Override
	public User validate(String uname, String password) {

User user=urepo.findByUnameAndPassword(uname, password);
		return user;
	}

}
