package com.niit.auth.service;

import com.niit.auth.model.User;

public interface UserDAO {
	
	User addUser(User obj);
	User validate(String uname,String Password);

}
