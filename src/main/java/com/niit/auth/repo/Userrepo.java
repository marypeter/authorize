package com.niit.auth.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.niit.auth.model.User;
@Repository
public interface Userrepo extends JpaRepository<User,String> {
	public User findByUnameAndPassword(String uname,String pwd);

}
